import io
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import View
from .models import encuesta, pregunta1, opciones1, opciones2, opciones3, opciones4, opciones5, opciones6, opciones7, opciones8, opciones9, opciones10, pregunta2, pregunta3, pregunta4, pregunta5, pregunta6, pregunta7, pregunta8, pregunta9, pregunta10
from apps.personas.models import Datos
from .forms import Op1Form, Op2Form, Op3Form, Op4Form, Op5Form, Op6Form, Op7Form, Op8Form, Op9Form, Op10Form
from django.http import FileResponse
from django.template.loader import get_template

#Reportlab libreria
import reportlab
from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.graphics.charts.legends import Legend
from reportlab.graphics.charts.piecharts import Pie
from reportlab.pdfbase.pdfmetrics import stringWidth, EmbeddedType1Face, registerTypeFace, Font, registerFont
from reportlab.graphics.shapes import Drawing, _DrawingEditorMixin, Rect
from reportlab.lib.colors import PCMYKColor
from reportlab.graphics import renderPDF
from reportlab.lib.pagesizes import A4
from reportlab.graphics.charts.lineplots import LinePlot
from reportlab.graphics.charts.barcharts import VerticalBarChart
from reportlab.graphics.widgets.markers import makeMarker




def listarpreguntas(request, id):
    pre1 = pregunta1.objects.all()
    pre2 = pregunta2.objects.all()
    pre3 = pregunta3.objects.all()
    pre4 = pregunta4.objects.all()
    pre5 = pregunta5.objects.all()
    pre6 = pregunta6.objects.all()
    pre7 = pregunta7.objects.all()
    pre8 = pregunta8.objects.all()
    pre9 = pregunta9.objects.all()
    pre10 = pregunta10.objects.all()
    usuario = Datos.objects.get(id = id)
    if request.method == 'POST':
        op1_form = Op1Form(request.POST)
        op2_form = Op2Form(request.POST)
        op3_form = Op3Form(request.POST)
        op4_form = Op4Form(request.POST)
        op5_form = Op5Form(request.POST)
        op6_form = Op6Form(request.POST)
        op7_form = Op7Form(request.POST)
        op8_form = Op8Form(request.POST)
        op9_form = Op9Form(request.POST)
        op10_form = Op10Form(request.POST)
        if op1_form.is_valid():
            op1_form.save()
            op2_form.save()
            op3_form.save()
            op4_form.save()
            op5_form.save()
            op6_form.save()
            op7_form.save()
            op8_form.save()
            op9_form.save()
            op10_form.save()
            return redirect('personas:listarpersona')
    else:
        op1_form = Op1Form()
        op2_form = Op2Form()
        op3_form = Op3Form()
        op4_form = Op4Form()
        op5_form = Op5Form()
        op6_form = Op6Form()
        op7_form = Op7Form()
        op8_form = Op8Form()
        op9_form = Op9Form()
        op10_form = Op10Form()


    return render (request, 'opciones/listar.html', {'pre1': pre1, 'op1_form': op1_form, 'pre2': pre2, 'op2_form': op2_form, 'pre3': pre3, 'op3_form': op3_form, 'pre4': pre4,  'op4_form': op4_form,'pre5': pre5, 'op5_form': op5_form, 'pre6': pre6, 'op6_form': op6_form,  'pre7': pre7,  'op7_form': op7_form, 'pre8': pre8, 'op8_form': op8_form, 'pre9': pre9, 'op9_form': op9_form, 'pre10': pre10,  'op10_form': op10_form, 'usuario': usuario.id})

def respuestas(request):
    pre1 = list(pregunta1.objects.all())
    res1 = list(opciones1.objects.values('opcion'))
    res1_op1 = res1.count({'opcion': 'Poco Saludable'})
    res1_op2 = res1.count({'opcion': 'Regular Saludable'})
    res1_op3 = res1.count({'opcion': 'Saludable'})
    res1_op4 = res1.count({'opcion': 'Muy Saludable'})
    res1_op5 = res1.count({'opcion': 'Excelente Saludable'})    

    return render (request, 'respuestas.html', {'pre1': pre1[0],'res1': res1, 'res1_op1': res1_op1, 'res1_op2': res1_op2, 'res1_op3': res1_op3, 'res1_op4': res1_op4, 'res1_op5': res1_op5})

class reporte(View):  
     
    def cabecera(self,pdf):
        #Establecemos el tamaño de letra en 16 y el tipo de letra Helvetica
        pdf.setFont("Helvetica", 16)
        #Dibujamos una cadena en la ubicación X,Y especificada
        pdf.drawString(120, 790, u"Sociedad Guatemalteca de Talento Humano")
        pdf.setFont("Helvetica", 14)
        pdf.drawString(200, 770, u"Encuesta de Salud")
               
         
    def get(self, request, *args, **kwargs):
        #Indicamos el tipo de contenido a devolver, en este caso un pdf
        response = HttpResponse(content_type='application/pdf')
        #La clase io.BytesIO permite tratar un array de bytes como un fichero binario, se utiliza como almacenamiento temporal
        buffer = BytesIO()
        #Canvas nos permite hacer el reporte con coordenadas X y Y
        pdf = canvas.Canvas(buffer, pagesize=A4)
        #Llamo al método cabecera donde están definidos los datos que aparecen en la cabecera del reporte.
        pre1 = list(pregunta1.objects.all())
        res1 = list(opciones1.objects.values('opcion'))
        res1_op1 = res1.count({'opcion': 'Poco Saludable'})
        res1_op2 = res1.count({'opcion': 'Regular Saludable'})
        res1_op3 = res1.count({'opcion': 'Saludable'})
        res1_op4 = res1.count({'opcion': 'Muy Saludable'})
        res1_op5 = res1.count({'opcion': 'Excelente Saludable'})  
        self.cabecera(pdf) 
        pdf.setFont("Helvetica", 12)
        d = Drawing(20, 20)
        bar = VerticalBarChart()
        bar.x = 50
        bar.y = 50
        op1 = ['PS', 'RS', 'S', 'MS', 'ES']
        bar.data = [[res1_op1],[res1_op2], [res1_op3], [res1_op4], [res1_op5]]
        bar.categoryAxis.categoryNames = op1

        bar.width = 200
        bar.height = 200
        bar.valueAxis.rangeRound = 'both'
        bar.valueAxis.valueMax = 10
        bar.valueAxis.forceZero = 1
        bar.bars.fillColor   = PCMYKColor(100,200,100,40,alpha=45)
    
        d.add(bar, '')
        pdf.setFont("Helvetica", 12)
        pdf.drawString(75,700, str(pre1[0]))


        pdf.setTitle("Comparacion de Resultados")
        renderPDF.draw(d, pdf, 150, 400)   
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response


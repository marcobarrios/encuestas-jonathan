from django.urls import path
from .views import listarpreguntas, respuestas, reporte

urlpatterns = [
    path ('listar/<int:id>', listarpreguntas, name= 'listarpreguntas'),
    path('respuestas', respuestas, name='respuestas'),
    path('pdf/', reporte.as_view(), name='pdf'), 
]
from django import forms
from .models import opciones1, opciones2, opciones3, opciones4, opciones5, opciones6, opciones7, opciones8, opciones9, opciones10

class Op1Form(forms.ModelForm):
    class Meta:
        model = opciones1
        fields = ['encuesta','opcion']

class Op2Form(forms.ModelForm):
    class Meta:
        model = opciones2
        fields = ['encuesta2','opcion2']

class Op3Form(forms.ModelForm):
    class Meta:
        model = opciones3
        fields = ['encuesta3','opcion3']

class Op4Form(forms.ModelForm):
    class Meta:
        model = opciones4
        fields = ['encuesta4','opcion4']

class Op5Form(forms.ModelForm):
    class Meta:
        model = opciones5
        fields = ['encuesta5','opcion5']

class Op6Form(forms.ModelForm):
    class Meta:
        model = opciones6
        fields = ['encuesta6','opcion6']

class Op7Form(forms.ModelForm):
    class Meta:
        model = opciones7
        fields = ['encuesta7','opcion7']

class Op8Form(forms.ModelForm):
    class Meta:
        model = opciones8
        fields = ['encuesta8','opcion8']

class Op9Form(forms.ModelForm):
    class Meta:
        model = opciones9
        fields = ['encuesta9','opcion9']

class Op10Form(forms.ModelForm):
    class Meta:
        model = opciones10
        fields = ['encuesta10','opcion10']
# Generated by Django 2.2.5 on 2019-09-15 20:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='encuesta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_encuesta', models.CharField(max_length=100)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Encuesta',
                'verbose_name_plural': 'Encuestas',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones1',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta', models.IntegerField()),
                ('opcion', models.CharField(choices=[('Poco Saludable', 'Poco Saludable'), ('Regular Saludable', 'Regular Saludable'), ('Saludable', 'Saludable'), ('Muy Saludable', 'Muy Saludable'), ('Excelente Saludable', 'Excelente Saludable')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 1',
                'verbose_name_plural': 'Respuesta 1',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones10',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta10', models.IntegerField()),
                ('opcion10', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 10',
                'verbose_name_plural': 'Respuesta 10',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta2', models.IntegerField()),
                ('opcion2', models.CharField(choices=[('SI', 'SI'), ('NO', 'NO')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 2',
                'verbose_name_plural': 'Respuesta 2',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones3',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta3', models.IntegerField()),
                ('opcion3', models.CharField(choices=[('Presión alterial Alta', 'Presion alterial Alta'), ('Diabetes', 'Diabetes'), ('Hemolifia', 'Hemofilia'), ('Otro', 'Otro'), ('Ninguna', 'Ninguna')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 3',
                'verbose_name_plural': 'Respuesta 3',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones4',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta4', models.IntegerField()),
                ('opcion4', models.CharField(choices=[('Si, a ambos', 'Si, a ambos'), ('Solo a las Drogas', 'Solo a las Drogas'), ('Solo al alcohol', 'Solo al alcohol'), ('Ninguna', 'Ninguna')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 4',
                'verbose_name_plural': 'Respuesta 4',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones5',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta5', models.IntegerField()),
                ('opcion5', models.CharField(choices=[('Una vez cada 3 meses', 'Una vez cada 3 meses'), ('Una vez cada 6 meses', 'Una vez cada 6 meses'), ('Una vez cada al año', 'Una vez al año'), ('Solo cuando es necesario', 'Solo cuando es necesario'), ('Nunca lo hago', 'Nunca lo hago')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 5',
                'verbose_name_plural': 'Respuesta 5',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones6',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta6', models.IntegerField()),
                ('opcion6', models.CharField(choices=[('Excelente', 'Excelente'), ('Muy Bien', 'Muy Bien'), ('Promedio', 'Promedio'), ('Mal', 'Mal'), ('Muy mal', 'Muy mal')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 6',
                'verbose_name_plural': 'Respuesta 6',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones7',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta7', models.IntegerField()),
                ('opcion7', models.CharField(choices=[('Excelente Capacidad', 'Excelente Capacidad'), ('Buena Capacidad', 'Buena Capacidad'), ('Capacidad Moderada', 'Capacidad Moderada'), ('Deterioro Grave Capacidad', 'Deterioro Grave Capacidad'), ('Deterioro total de la Capacidad', 'Deterioro total de la Capacidad')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 7',
                'verbose_name_plural': 'Respuesta 7',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones9',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta9', models.IntegerField()),
                ('opcion9', models.CharField(choices=[('Completamente desacuerdo', 'Completamente desacuerdo'), ('Algo en desacuerdo', 'Algo en desacuerdo'), ('Neutral', 'Neutral'), ('Un poco acuerdo', 'Un poco acuerdo'), ('Completamente acuerdo', 'Completamente acuerdo')], max_length=100)),
            ],
            options={
                'verbose_name': 'Respuesta 9',
                'verbose_name_plural': 'Respuesta 9',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta9',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('encuesta9', models.IntegerField()),
                ('texto', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Pregunta 9',
                'verbose_name_plural': 'Pregunta 9',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta8',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta8', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 8',
                'verbose_name_plural': 'Pregunta 8',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta7',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta7', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 7',
                'verbose_name_plural': 'Pregunta 7',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta6',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta6', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 6',
                'verbose_name_plural': 'Pregunta 6',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta5',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta5', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 5',
                'verbose_name_plural': 'Pregunta 5',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta4',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta4', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 4',
                'verbose_name_plural': 'Pregunta 4',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta3',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta3', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 3',
                'verbose_name_plural': 'Pregunta 3',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta2', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 2',
                'verbose_name_plural': 'Pregunta 2',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta10',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta10', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 10',
                'verbose_name_plural': 'Pregunta 10',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='pregunta1',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=100)),
                ('encuesta1', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Pregunta 1',
                'verbose_name_plural': 'Pregunta 1',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='opciones8',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('opcion8', models.CharField(choices=[('Sin ayuda', 'Sin ayuda'), ('Con algo de ayuda', 'Con algo de ayuda'), ('Completamente con ayuda', 'Completamente con ayuda')], max_length=100)),
                ('encuesta8', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='opciones.encuesta')),
            ],
            options={
                'verbose_name': 'Respuesta 8',
                'verbose_name_plural': 'Respuesta 8',
                'ordering': ['id'],
            },
        ),
    ]

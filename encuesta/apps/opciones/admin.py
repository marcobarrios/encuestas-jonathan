from django.contrib import admin
from .models import encuesta, pregunta1, opciones1, pregunta2, opciones2, pregunta3, opciones3, pregunta4, opciones4, pregunta5, opciones5, pregunta6, opciones6, pregunta7, opciones7, pregunta8, opciones8, pregunta9, opciones9, pregunta10, opciones10


# Register your models here.
admin.site.register(encuesta)
admin.site.register(pregunta1)
admin.site.register(opciones1)
admin.site.register(pregunta2)
admin.site.register(opciones2)
admin.site.register(pregunta3)
admin.site.register(opciones3)
admin.site.register(pregunta4)
admin.site.register(opciones4)
admin.site.register(pregunta5)
admin.site.register(opciones5)
admin.site.register(pregunta6)
admin.site.register(opciones6)
admin.site.register(pregunta7)
admin.site.register(opciones7)
admin.site.register(pregunta8)
admin.site.register(opciones8)
admin.site.register(pregunta9)
admin.site.register(opciones9)
admin.site.register(pregunta10)
admin.site.register(opciones10)

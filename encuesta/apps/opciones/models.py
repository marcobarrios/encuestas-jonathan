from django.db import models
from apps.personas.models import Datos

# Create your models here.
class encuesta(models.Model):
    nombre_encuesta = models.CharField(max_length=100, blank=False, null=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nombre_encuesta

    class Meta:
        verbose_name = 'Encuesta'
        verbose_name_plural = 'Encuestas'
        ordering = ['id']

class pregunta1(models.Model):
    encuesta1 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 1'
        verbose_name_plural = 'Pregunta 1'
        ordering = ['id']



class opciones1(models.Model):
    opciones = (
        ('Poco Saludable', 'Poco Saludable'),
        ('Regular Saludable', 'Regular Saludable'),
        ('Saludable', 'Saludable'),
        ('Muy Saludable', 'Muy Saludable'),
        ('Excelente Saludable', 'Excelente Saludable')
    )
    encuesta = models.IntegerField(blank=False, null=False)
    opcion = models.CharField(max_length=100, choices=opciones)
    
    def __str__(self):
        return self.opcion
    
    class Meta:
        verbose_name = 'Respuesta 1'
        verbose_name_plural = 'Respuesta 1'
        ordering = ['id']

class pregunta2(models.Model):
    encuesta2 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 2'
        verbose_name_plural = 'Pregunta 2'
        ordering = ['id']

class opciones2(models.Model):
    opciones = (
        ('SI', 'SI'),
        ('NO', 'NO')
    )
    encuesta2 = models.IntegerField(blank=False, null=False)
    opcion2 = models.CharField(max_length=100, choices=opciones)

    def __str__(self):
        return self.opcion2
    
    class Meta:
        verbose_name = 'Respuesta 2'
        verbose_name_plural = 'Respuesta 2'
        ordering = ['id']

class pregunta3(models.Model):
    encuesta3 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 3'
        verbose_name_plural = 'Pregunta 3'
        ordering = ['id']

class opciones3(models.Model):
    opciones = (
        ('Presión alterial Alta', 'Presion alterial Alta'),
        ('Diabetes', 'Diabetes'),
        ('Hemolifia', 'Hemofilia'),
        ('Otro', 'Otro'),
        ('Ninguna', 'Ninguna')
    )
    encuesta3 = models.IntegerField(blank=False, null=False)
    opcion3 = models.CharField(max_length=100, choices=opciones)

    def __str__(self):
        return self.opcion3

    
    class Meta:
        verbose_name = 'Respuesta 3'
        verbose_name_plural = 'Respuesta 3'
        ordering = ['id']

class pregunta4(models.Model):
    encuesta4 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 4'
        verbose_name_plural = 'Pregunta 4'
        ordering = ['id']

class opciones4(models.Model):
    opciones = (
        ('Si, a ambos', 'Si, a ambos'),
        ('Solo a las Drogas', 'Solo a las Drogas'),
        ('Solo al alcohol', 'Solo al alcohol'),
        ('Ninguna', 'Ninguna')
    )
    encuesta4 = models.IntegerField(blank=False, null=False)
    opcion4 = models.CharField(max_length=100, choices=opciones)

    def __str__(self):
        return self.opcion4

    class Meta:
        verbose_name = 'Respuesta 4'
        verbose_name_plural = 'Respuesta 4'
        ordering = ['id']

class pregunta5(models.Model):
    encuesta5 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 5'
        verbose_name_plural = 'Pregunta 5'
        ordering = ['id']

class opciones5(models.Model):
    opciones = (
        ('Una vez cada 3 meses', 'Una vez cada 3 meses'),
        ('Una vez cada 6 meses', 'Una vez cada 6 meses'),
        ('Una vez cada al año', 'Una vez al año'),
        ('Solo cuando es necesario', 'Solo cuando es necesario'),
        ('Nunca lo hago', 'Nunca lo hago')
    )
    encuesta5 = models.IntegerField(blank=False, null=False)
    opcion5 = models.CharField(max_length=100, choices=opciones)
    

    def __str__(self):
        return self.opcion5

    class Meta:
        verbose_name = 'Respuesta 5'
        verbose_name_plural = 'Respuesta 5'
        ordering = ['id']

class pregunta6(models.Model):
    encuesta6 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 6'
        verbose_name_plural = 'Pregunta 6'
        ordering = ['id']

class opciones6(models.Model):
    opciones = (
        ('Excelente', 'Excelente'),
        ('Muy Bien', 'Muy Bien'),
        ('Promedio', 'Promedio'),
        ('Mal', 'Mal'),
        ('Muy mal', 'Muy mal')
    )
    encuesta6 = models.IntegerField(blank=False, null=False)
    opcion6 = models.CharField(max_length=100, choices=opciones)
    
    def __str__(self):
        return self.opcio6

    class Meta:
        verbose_name = 'Respuesta 6'
        verbose_name_plural = 'Respuesta 6'
        ordering = ['id']

class pregunta7(models.Model):
    encuesta7 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 7'
        verbose_name_plural = 'Pregunta 7'
        ordering = ['id']

class opciones7(models.Model):
    opciones = (
        ('Excelente Capacidad', 'Excelente Capacidad'),
        ('Buena Capacidad', 'Buena Capacidad'),
        ('Capacidad Moderada', 'Capacidad Moderada'),
        ('Deterioro Grave Capacidad', 'Deterioro Grave Capacidad'),
        ('Deterioro total de la Capacidad', 'Deterioro total de la Capacidad')
    )
    encuesta7 = models.IntegerField(blank=False, null=False)
    opcion7 = models.CharField(max_length=100, choices=opciones)
    
    def __str__(self):
        return self.opcion7
    
    class Meta:
        verbose_name = 'Respuesta 7'
        verbose_name_plural = 'Respuesta 7'
        ordering = ['id']

class pregunta8(models.Model):
    encuesta8 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 8'
        verbose_name_plural = 'Pregunta 8'
        ordering = ['id']

class opciones8(models.Model):
    opciones = (
        ('Sin ayuda', 'Sin ayuda'),
        ('Con algo de ayuda', 'Con algo de ayuda'),
        ('Completamente con ayuda', 'Completamente con ayuda')
    )
    encuesta8 = models.IntegerField(blank=False, null=False)
    opcion8 = models.CharField(max_length=100, choices=opciones)

    def __str__(self):
        return self.opcion8

    class Meta:
        verbose_name = 'Respuesta 8'
        verbose_name_plural = 'Respuesta 8'
        ordering = ['id']

class pregunta9(models.Model):
    encuesta9 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 9'
        verbose_name_plural = 'Pregunta 9'
        ordering = ['id']

class opciones9(models.Model):
    opciones = (
        ('Completamente desacuerdo', 'Completamente desacuerdo'),
        ('Algo en desacuerdo', 'Algo en desacuerdo'),
        ('Neutral', 'Neutral'),
        ('Un poco acuerdo', 'Un poco acuerdo'),
        ('Completamente acuerdo', 'Completamente acuerdo')
    )
    encuesta9 = models.IntegerField(blank=False, null=False)
    opcion9 = models.CharField(max_length=100, choices=opciones)

    def __str__(self):
        return self.opcion9

    class Meta:
        verbose_name = 'Respuesta 9'
        verbose_name_plural = 'Respuesta 9'
        ordering = ['id']

class pregunta10(models.Model):
    encuesta10 = models.ForeignKey(encuesta, on_delete=models.CASCADE)
    texto = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.texto
    
    class Meta:
        verbose_name = 'Pregunta 10'
        verbose_name_plural = 'Pregunta 10'
        ordering = ['id']

class opciones10(models.Model):
    opciones = (
        ('SI', 'SI'),
        ('NO', 'NO'),
    )
    encuesta10 = models.IntegerField(blank=False, null=False)
    opcion10 = models.CharField(max_length=100, choices=opciones)
    
    def __str__(self):
        return self.opcion10
    
    class Meta:
        verbose_name = 'Respuesta 10'
        verbose_name_plural = 'Respuesta 10'
        ordering = ['id']





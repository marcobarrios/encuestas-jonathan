from django.urls import path
from .views import crearpersona, listarpersona, editarpersona, disablepersona


urlpatterns = [
    path ('crear/', crearpersona, name= 'crearpersona'),
    path ('listar/', listarpersona, name= 'listarpersona'),
    path('editar/<int:id>', editarpersona, name='editarpersona'),
    path('eliminar/<int:id>', disablepersona, name='disablepersona')

]
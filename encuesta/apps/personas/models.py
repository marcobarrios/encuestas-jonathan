from django.db import models
from django_countries.fields import CountryField


# Create your models here.
class Datos(models.Model):
    Opcion_Sexo = (
        ('Hombre', 'Hombre'),
        ('Mujer', 'Mujer'),
    )
    estado_civil = (
        ('Soltero', 'Soltero'),
        ('Casado', 'Casado'),
        ('Divorciado', 'Divorciado'),
        ('Viudo', 'Viudo')
    )
    nombres = models.CharField(max_length=100, blank = False, null= False)
    apellidos = models.CharField(max_length=100, blank = False, null= False)
    edad = models.IntegerField(blank= False, null= False)
    dpi = models.IntegerField(blank=False, null= False)
    sexo = models.CharField(max_length= 10, choices=Opcion_Sexo)
    nacionalidad = CountryField(blank_label='Escoger Pais')
    ecivil = models.CharField(max_length=10, choices=estado_civil)
    estado = models.BooleanField('Estado', default=True)

    def __str__(self):
        return self.nombres 

    class Meta:
        verbose_name = 'Datos'
        verbose_name_plural = 'Datos'
        ordering = ['id']



from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from .forms import DatosForm
from .models import Datos
from apps.opciones.models import opciones1

# Create your views here.
def home(request):
    return render(request, 'index.html')

def crearpersona(request):
    if request.method == 'POST':
        datos_form = DatosForm(request.POST)
        if datos_form.is_valid():
            datos_form.save()
            return redirect('personas:listarpersona')
    else:
        datos_form = DatosForm()
    
    return render(request, 'personas/crear.html', {'datos_form': datos_form})

def listarpersona(request):
    persona = Datos.objects.filter(estado = True)
    return render(request, 'personas/listar.html', {'persona': persona})

def editarpersona(request, id):
    datos_form = None
    error = None
    try:
        eperson = Datos.objects.get(id = id)
        if request.method == 'GET':
            datos_form = DatosForm(instance= eperson)
        else:
            datos_form = DatosForm(request.POST, instance= eperson)
            if datos_form.is_valid():
                datos_form.save()
                return redirect('personas:listarpersona')

    except ObjectDoesNotExist as e:
        error = e

    return render(request, 'personas/crear.html', {'datos_form': datos_form, 'error': error})

def disablepersona(request, id):
    dperson = Datos.objects.get(id = id)
    dperson.estado = False
    dperson.save()
    return redirect('personas:listarpersona')

from django import forms
from .models import Datos

class DatosForm(forms.ModelForm):
    class Meta:
        model = Datos
        fields = ['nombres', 'apellidos', 'edad', 'dpi', 'sexo', 'nacionalidad', 'ecivil']
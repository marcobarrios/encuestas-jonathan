from django.contrib import admin
from django.urls import include, path
from apps.personas.views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('personas/', include(('apps.personas.urls', 'personas'))),
    path('opciones/', include(('apps.opciones.urls', 'opciones'))),
    path('', home, name='index')
]